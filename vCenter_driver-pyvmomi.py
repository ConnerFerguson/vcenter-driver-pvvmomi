#!/usr/bin/env python
# Copyright (c) 2014 Marist SDN Innovation lab Joint with Plexxi Inc.
# All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#

from pyVim.connect import SmartConnect
import pyVmomi
import sys

from vconnector.core import VConnector
from congress.datasources import datasource_utils
from congress.datasources.datasource_driver import DataSourceDriver
from congress.openstack.common import log as logging

LOG = logging.getLogger(__name__)


def d6service(name, keys, inbox, datapath, args):
    """This method is called by d6cage to create a dataservice
    instance.
    """
    return VCenterDriver(name, keys, inbox, datapath, args)


class VCenterDriver(DataSourceDriver):
    HOSTS = "hosts"
    VMS = "vms"
    HOST_VMACS = "host.vnic_macs"
    HOST_PMACS = "host.pnic_macs"
    HOST_DNS = "Host.DNS_IPs"

    value_trans = {'translation-type': 'VALUE'}

    vms_translator = {
        'translation-type': 'HDICT',
        'table-name': VMS,
        'selector-type': 'DICT_SELECTOR',
        'field-translators':
            ({'fieldname': 'name', 'translator': value_trans},
             {'fieldname': 'uuid', 'translator': value_trans},
             {'fieldname': 'file_path', 'translator': value_trans},
             {'fieldname': 'disk_size', 'translator': value_trans},
             {'fieldname': 'mac', 'translator': value_trans},
             {'fieldname': 'datastore', 'translator': value_trans},
             {'fieldname': 'annotation', 'translator': value_trans},
             {'fieldname': 'host_uuid', 'translator': value_trans})}

    hosts_translator = {
        'translation-type': 'HDICT',
        'table-name': HOSTS,
        'selector-type': 'DICT_SELECTOR',
        'field-translators':
            ({'fieldname': 'name', 'translator': value_trans},
             {'fieldname': 'uuid', 'translator': value_trans},
             {'fieldname': HOST_DNS, 'col': 'Host:DNS_id',
              'translator': {'translation-type': 'LIST',
                             'table-name': HOST_DNS,
                             'id-col': 'Host:DNS_id',
                             'val-col': 'DNS_IPs',
                             'translator': value_trans}},
             {'fieldname': 'vnic_macs',
              'col': 'host:vnic_mac_id',
              'translator': {'translation-type': 'LIST',
                             'table-name': HOST_VMACS,
                             'id-col': 'host:vnic_mac_id',
                             'val-col': 'vnic_mac_addresses',
                             'translator': value_trans}},
             {'fieldname': 'pnic_macs',
              'col': 'host:pnic_mac_id',
              'translator': {'translation-type': 'LIST',
                             'table-name': HOST_PMACS,
                             'id-col': 'host:pnic_mac_id',
                             'val-col': 'pnic_mac_addresses',
                             'translator': value_trans}})}

    def __init__(self, name='', keys='', inbox=None, datapath=None, args=None,
                 session=None):
        if args is None:
            args = self.empty_credentials()
        else:
            args['tenant_name'] = None
        super(VCenterDriver, self).__init__(name, keys, inbox, datapath, args)
        self.register_translator(VCenterDriver.hosts_translator)
        self.register_translator(VCenterDriver.vms_translator)
        self.__connection = None
        self.raw_state = {}
        self.creds = datasource_utils.get_credentials(name, args)
        self._si = VConnector(user=self.creds['username'],
                              pwd=self.creds['password'],
                              host=self.creds['auth_url'])
        self._si.connect()
        if not self.connect():
            sys.exit(1)
        else:
            self.initialized = True

    def update_from_datasource(self):
        """Called when it is time to pull new data from this datasource.
        Sets self.state[tablename] = <list of tuples of strings/numbers>
        for every tablename exported by vCenter.
        """

        data = self.getData()
        if (self.HOSTS not in self.raw_state or
           data['hostList'] != self.raw_state[self.HOSTS]):
            hosts = self.organizeHosts(data['hostList'])
            self._translateHosts(hosts)
            self.raw_state[self.HOSTS] = data['hostList']
        else:
            self.hosts = self.state[self.HOSTS]

        if (self.VMS not in self.raw_state or
           data['vmList'] != self.raw_state[self.VMS]):
            vms = self.organizeVMs(data['vmList'])
            self._translateVMs(vms)
            self.raw_state[self.VMS] = data['vmList']
        else:
            self.vms = self.state[self.VMS]

    def organizeHosts(self, data):
        d = []
        for host in data:
            h = {}
            h['name'] = host['name']
            h['uuid'] = host['hardware.systemInfo.uuid']
            h[self.HOST_DNS] = host['config.network.dnsConfig.address']
            h['vnic_macs'] = []
            h['pnic_macs'] = []
            try:
                for vnic in host['config.network.vnic']:
                    if vnic.spec.mac != "00:00:00:00:00:00":
                        h['vnic_macs'].append(vnic.spec.mac)
            except AttributeError:
                pass
            try:
                for pnic in host['config.network.pnic']:
                    if pnic.mac != "00:00:00:00:00:00":
                        h['pnic_macs'].append(pnic.mac)
            except AttributeError:
                pass
            d.append(h)
        return d

    def organizeVMs(self, data):
        d = []
        virdisk = "<class 'pyVmomi.VmomiSupport.vim.vm.device.VirtualDisk'>"
        vmnet = "<class 'pyVmomi.VmomiSupport.vim.vm.device.VirtualVmxnet3'>"
        for vm in data:
            v = {}
            try:
                v['name'] = vm['name']
                v['uuid'] = vm['config.uuid']
                v['annotation'] = vm['config.annotation']
                v['host_uuid'] = vm['runtime.host'].hardware.systemInfo.uuid
                devs = vm['config.hardware.device']
                for dev in devs:
                    if str(type(dev)) == virdisk:
                        v['disk_size'] = dev.capacityInKB
                        v['file_path'] = dev.backing.fileName
                        v['datastore'] = dev.backing.datastore
                    elif str(type(dev)) == vmnet:
                        v['mac'] = dev.macAddress
                d.append(v)
            except KeyError:
                pass
        return d

    def _translateHosts(self, hosts):
        row_data = VCenterDriver.convert_objs(hosts,
                                              VCenterDriver.hosts_translator)
        host_tables = (self.HOSTS, self.HOST_DNS, self.HOST_VMACS,
                       self.HOST_PMACS)
        for table in host_tables:
            self.state[table] = set()
        for table, row in row_data:
            assert table in host_tables
            self.state[table].add(row)

    def _translateVMs(self, obj):
        row_data = VCenterDriver.convert_objs(obj,
                                              VCenterDriver.vms_translator)
        self.state[self.VMS] = set()
        for table, row in row_data:
            assert table == self.VMS
            self.state[table].add(row)

    def connect(self):
        """Connect to vCenter
        """
        connect = False
        try:
            self.__connection.CurrentTime()
        except AttributeError:
            connect = True
        except pyVmomi.vim.fault.NotAuthenticated:
            connect = True

        if connect:
            try:
                self.__connection = SmartConnect(
                    host=self.creds['auth_url'],
                    user=self.creds['username'],
                    pwd=self.creds['password'])
            except pyVmomi.vim.fault.NotAuthenticated:
                LOG.warning("Cannot connect to vCenter @ " +
                            self.creds['auth_url'] + ", not authenticated")
                return False

            LOG.info("Successfully connected to vCenter @ " +
                     self.creds['auth_url'])
        return True

    def getData(self):
        """
        Gets all the virtualization hosts from vCenter and returns them in a
        list of dictionaries.

        Format:
                {"name": hostname,
                 "uuid": hostuuid,
                 "ip": hostipaddresses,
                 "macaddress": hostmacaddress,
                 "virtualmachines": virtual machines on host}

        Returns:
            data (dict) -- HostSystems and VirtualMachines from vCenter
        """
        try:
            hosts = self._si.get_host_view()
        except pyVmomi.vim.fault.NotAuthenticated:
            self._si = VConnector(user=self.creds['username'],
                                  pwd=self.creds['password'],
                                  host=self.creds['auth_url'])
            self._si.connect()
            hosts = self._si.get_host_view()

        sets = ['name', 'hardware.systemInfo.uuid',
                'config.network.dnsConfig.address', 'config.network.vnic',
                'config.network.pnic',
                'config.network.portgroup',
                'network']

        try:
            hostlist = self._si.collect_properties(
                view_ref=hosts,
                obj_type=pyVmomi.vim.HostSystem,
                path_set=sets)
        except pyVmomi.vim.fault.NotAuthenticated:
            self._si = VConnector(user=self.creds['username'],
                                  pwd=self.creds['password'],
                                  host=self.creds['auth_url'])
            self._si.connect()
            hostlist = self._si.collect_properties(
                view_ref=hosts,
                obj_type=pyVmomi.vim.HostSystem,
                path_set=sets)

        try:
            networks = self._si.collect_properties(
                view_ref=hosts,
                obj_type=pyVmomi.vim.HostSystem,
                path_set=['config.network.portgroup'])
        except pyVmomi.vim.fault.NotAuthenticated:
            self._si = VConnector(user=self.creds['username'],
                                  pwd=self.creds['password'],
                                  host=self.creds['auth_url'])
            self._si.connect()
            networks = self._si.collect_properties(
                view_ref=hosts,
                obj_type=pyVmomi.vim.HostSystem,
                path_set=['config.network.portgroup'])

        try:
            vms = self._si.get_vm_view()
        except pyVmomi.vim.fault.NotAuthenticated:
            self._si = VConnector(user=self.creds['username'],
                                  pwd=self.creds['password'],
                                  host=self.creds['auth_url'])
            self._si.connect()
            vms = self._si.get_vm_view()

        sets = ['name', 'config.uuid', 'runtime.host', 'runtime.device',
                'config.hardware.device', 'network', 'config.annotation',
                'summary.config.vmPathName']

        try:
            vmlist = self._si.collect_properties(
                view_ref=vms,
                obj_type=pyVmomi.vim.VirtualMachine,
                path_set=sets)
        except pyVmomi.vim.fault.NotAuthenticated:
            self._si = VConnector(user=self.creds['username'],
                                  pwd=self.creds['password'],
                                  host=self.creds['auth_url'])
            self._si.connect()
            vmlist = self._si.collect_properties(
                view_ref=vms,
                obj_type=pyVmomi.vim.VirtualMachine,
                path_set=sets)

        data = {}
        data['hostList'] = hostlist
        data['vmList'] = vmlist
        data['networks'] = networks
        return data
