### What is this repository for? ###

The vCenter Driver is code written to integrate PlexxiCore with the Openstack Policy engine [Congress](https://wiki.openstack.org/wiki/Congress).  It allows congress to create and populate tables with data gained from vCenter for use in future Polices.  

This driver is using pyvmomi to connect to vCenter, there is an alternate version at https://bitbucket.org/ConnerFerguson/vcenter-driver using oslo.vmware instead in order to remove additional dependencies.

### How do I get set up? ###

You will first need congress installed to use the vCenterDriver. If you do not have Congress, you can find it with installation instructions at
https://github.com/stackforge/congress


Once you have congress running:

1) Install dependencies  

```
pip install --upgrade pyvmomi
pip install vconnector
```

2) Add the vCenter_Driver-pyvmomi.py file to your datasources folder in congress
```
path/to/congress/congress/datasources
```
3)Modify your datasources.conf that your congress server is using to use the vCenter Driver. In a standard devstack install this will be
```
/etc/congress/datasources.conf
```

Add:
```
[vCenter-P]
module:datasources/vCenter_driver-pyvmomi.py
username: vCenter_username
password: vCenter_password
auth_url:  vCenter_URL

```

After the driver has been configured, data from vCenter should be visible through the congress API.

### Who do I talk to? ###

You can contact me at conner.ferguson1@marist.edu